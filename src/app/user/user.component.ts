import { Component } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user',
  template: `
    <div class="menus">
      <a routerLink="changes" routerLinkActive="active">Perubahan</a>
      <!-- <a routerLink="notification" routerLinkActive="active">Notification</a> -->
      <a *ngIf="userService.isModerator"
         routerLink="admin" routerLinkActive="active">Admin</a>
    </div>
    <router-outlet></router-outlet>`,
  styleUrls: ['user.component.css'],
})
export class UserComponent {
  constructor(public userService: UserService) { }
}
