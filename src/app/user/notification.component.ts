import { Component } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-notification',
  template: `TBA Notification`,
})
export class UserNotificationComponent {
  constructor(private userService: UserService) { }
}
