import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { map, distinctUntilChanged, switchMap, shareReplay } from 'rxjs/operators';
import { USER_ROLE, Relawan, RsHistory, PlaceData } from '../rs/rs.component';
import { Observable, combineLatest } from 'rxjs';
import { UtilService } from '../util.service';

export interface UserDetails {
  user: Relawan;
  placeDataById: { [placeId: string]: PlaceData };
  hasPending: boolean;
}

@Component({
  selector: 'app-user-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class UserProfileComponent {
  userDetails$: Observable<UserDetails>;
  USER_ROLE = USER_ROLE;
  Object = Object;

  constructor(
    public userService: UserService,
    public utilService: UtilService,
    private route: ActivatedRoute,
  ) {
    const user$ = this.route.paramMap.pipe(
      map((params: ParamMap) => params.get('id')),
      distinctUntilChanged(),
      switchMap(uid => uid
        ? this.userService.getUser$(uid)
        : this.userService.user$));

    this.userDetails$ =
      combineLatest(user$, this.utilService.placeDataById$).pipe(
        map(([user, placeDataById]) => {
          for (const [placeId, value] of Object.entries(user.changes)) {
            for (const item of this.entries(value)) {
              if (this.isPending(item, placeDataById[placeId])) {
                return { user, placeDataById, hasPending: true };
              }
            }
          }
          return { user, placeDataById, hasPending: false };
        }),
        shareReplay(1)
      );
  }

  entries(rsHistory: RsHistory) {
    return Object.entries<{ value: string | number, ts: number }>(rsHistory);
  }

  isPending([key, latest], placeData?: PlaceData) {
    return !placeData?.latest[key] || latest.ts > placeData?.latest[key]?.ts;
  }
}
