import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { UtilService } from '../util.service';
import { Observable, combineLatest } from 'rxjs';
import { map, shareReplay, startWith } from 'rxjs/operators';
import { PlaceData } from '../rs/rs.component';

interface RecentRsData {
  placeId: string;
  name: string;
  longAddress: string;
  shortAddress: string;
  neededItemsCount: number;
  urgentItemsCount: number;
  distance: number;
  lastUpdateTs: number;
  updatedAgo: string;
}

@Component({
  selector: 'app-list-recent',
  templateUrl: './recent.component.html',
  styleUrls: ['./recent.component.css']
})
export class RecentComponent {
  static LIMIT_RECENT_RS = 8;

  recentRs$: Observable<RecentRsData[]>;
  activatingLocation = false;
  Date = Date;

  constructor(
    public utilService: UtilService,
    public userService: UserService
  ) {
    this.recentRs$ =
      combineLatest(
        utilService.allPlaceData$,
        userService.user$.pipe(startWith(null))
      ).pipe(
        map(([rsData, user]) => rsData
          .map<RecentRsData>(rs => {
            const lastUpdateTs = this.getLastUpdateTs(rs.placeData);
            return {
              placeId: rs.placeData.place.place_id,
              name: rs.placeData.place.name,
              longAddress: rs.placeData.place.formatted_address,
              shortAddress: this.buildShortAddress(rs.placeData.place.address_components),
              neededItemsCount: rs.totalKebutuhan,
              urgentItemsCount: rs.totalMendesak,
              distance: this.getDistance(user?.location, rs.placeData),
              lastUpdateTs,
              updatedAgo: utilService.timeAgo(lastUpdateTs)
            };
          })
          .sort((a, b) => {
            // Sort by nearest distance and last update timestamp.
            if (a.distance < b.distance) return -1;
            if (a.distance > b.distance) return 1;
            return b.lastUpdateTs - a.lastUpdateTs;
          })
          .slice(0, RecentComponent.LIMIT_RECENT_RS)
        ),
        shareReplay(1)
      );
  }

  activateLocation(enable_location: boolean) {
    this.activatingLocation = true;
    return this.userService.post('enable_location', { enable_location })
      .then(() => this.activatingLocation = false);
  }

  private getDistance(userLocation: { lat: number, lng: number } | null, placeData: PlaceData) {
    return !userLocation ? 0 : this.utilService.calculateDistance(
      userLocation,
      { // RS location.
        lat: placeData.place.geometry.location.lat,
        lng: placeData.place.geometry.location.lng
      }
    );
  }

  private getLastUpdateTs(placeData: PlaceData) {
    return Object.keys(placeData.latest)
      .map(l => placeData.latest[l].ts)
      .reduce((prev, cur) => cur === undefined ? prev : Math.min(prev, cur));
  }

  private buildShortAddress(address_components): string {
    const provinsi = address_components.filter(addr => addr.types.indexOf('administrative_area_level_1') > -1)[0]['short_name'];
    const kabupaten = address_components.filter(addr => addr.types.indexOf('administrative_area_level_2') > -1)[0]['short_name'];
    const kecamatan = address_components.filter(addr => addr.types.indexOf('administrative_area_level_3') > -1)[0]['short_name'];
    return `${kecamatan}, ${kabupaten}, ${provinsi}`;
  }
}
