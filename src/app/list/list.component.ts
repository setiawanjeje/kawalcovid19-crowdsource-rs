import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { UtilService } from '../util.service';
import { ITEM_STATUS } from '../rs/rs.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {
  Object = Object;
  ITEM_STATUS = ITEM_STATUS;

  constructor(
    public utilService: UtilService,
    public userService: UserService,
  ) {
  }
}
