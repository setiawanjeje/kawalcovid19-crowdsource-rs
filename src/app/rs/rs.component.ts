export enum NUMBER_DATA_KEY {
  // Suspect
  odp = 'odp',
  pdpp = 'pdpp',
  pdpr = 'pdpr',
  pdps = 'pdps',
  covp = 'covp',
  covr = 'covr',
  covs = 'covs',

  // APDs
  maskn95 = 'maskn95',
  maskSurgical = 'maskSurgical',
  gloves = 'gloves',
  glovesSteril = 'glovesSteril',
  glovesLong = 'glovesLong',
  cap = 'cap',
  goggle = 'goggle',
  faceShield = 'faceShield',
  surgicalGown = 'surgicalGown',
  coverall = 'coverall',
  hazmats = 'hazmats',
  apron = 'apron',
  boots = 'boots',
  shoeCover = 'shoeCover',
  handSanitizer = 'handSanitizer',
  handSoap = 'handSoap',
  termometerInfrared = 'termometerInfrared',
  ventilator = 'ventilator',

  // Drugs
  vitaminC = 'vitaminC',
  vitaminE = 'vitaminE',

  // Rooms
  icu = 'icu',
  isolasi = 'isolasi',
  kondisi = 'kondisi',

  // Available Tools | Stock for Isolation Room
  availableVentilator = 'availableVentilator',
  availableMechanicalVentilator = 'availableMechanicalVentilator',
  availableEcmo = 'availableEcmo',
}

export enum STRING_DATA_KEY {
  contact = 'contact'
}

export enum ITEM_STATUS {
  unset = 'unset',  // Status is unset (default).
  out = 'out',      // Out of stock, urgent status.
  short = 'short',  // Short supply, need the items in 14 days.
  ok = 'ok',        // Items no longer needed.
}

export type HistoryValue<T> = {
  status: ITEM_STATUS,  // The urgency status of the item.
  value: T,             // The value of the edit.
  uid: string,          // The user who made the edit.
  ts: number,           // The timestamp of the edit (in milliseconds).
};

export type HistoryStrings = { [key in STRING_DATA_KEY]: HistoryValue<string>[] };
export type HistoryNumbers = { [key in NUMBER_DATA_KEY]: HistoryValue<number>[] };
export interface History extends HistoryStrings, HistoryNumbers { }

export type HistoryString = { [key in STRING_DATA_KEY]: HistoryValue<string> };
export type HistoryNumber = { [key in NUMBER_DATA_KEY]: HistoryValue<number> };
export interface LatestHistory extends HistoryString, HistoryNumber { }

export function isNumberDataKey(key: string): key is keyof typeof NUMBER_DATA_KEY {
  return key in NUMBER_DATA_KEY;
}

export function isStringDataKey(key: string): key is keyof typeof STRING_DATA_KEY {
  return key in STRING_DATA_KEY;
}

export interface PlaceData {
  // Google Map's Place Details.
  place: google.maps.places.PlaceResult;

  // Changes to this place done by Relawan.
  history: History;

  // Contains the latest value in the history (computed locally).
  latest: LatestHistory;
}

export function isHistoryKey(k: string): k is STRING_DATA_KEY | NUMBER_DATA_KEY {
  return k in STRING_DATA_KEY || k in NUMBER_DATA_KEY;
}

// Compute the latest value from PlaceData history.
// The userChanges may override PlaceData.
export function toPlaceData(obj: any, relawan: Relawan | null = null): PlaceData {
  const placeData = { ...obj, latest: {} } as PlaceData;
  for (const [k, v] of Object.entries<any[]>(obj.history ?? {})) {
    if (isHistoryKey(k)) {
      placeData.latest[k] = v[v.length - 1];
    }
  }
  for (const [placeId, history] of Object.entries(relawan?.changes || {})) {
    if (placeData.place.place_id !== placeId) continue;
    for (const [key, { status, value, ts }] of Object.entries(history)) {
      if (!isHistoryKey(key)) continue;
      if (!placeData.latest[key] || placeData.latest[key].ts < ts) {
        // @ts-ignore
        placeData.latest[key] = { status, value, ts };
      }
    }
  }
  return placeData;
}

export type RsHistory = {
  [key: string]: {
    status: ITEM_STATUS,
    value: string | number,
    ts: number
  }
}

export type UserChanges = { [placeId: string]: RsHistory };

export enum USER_ROLE {
  BANNED = -1,    // Can view tabulation (this is equivalent to not-logged in users).
  PUBLIC = 0,     // Can search and edit RS data in their workspace (not visible globally until approved).
  RELAWAN = 1,    // Can search and edit RS data globally (all their workspace edit becomes global).
  MODERATOR = 2,  // Can promote PUBLIC -> RELAWAN.
  ADMIN = 3,      // Can promote PUBLIC/RELAWAN -> MODERATOR.
}

export function computeUserChangesCount(relawan: Relawan, placeById?: { [placeId: string]: PlaceData }) {
  relawan.numRsChanged = 0;
  relawan.numFieldsChanged = 0;
  for (const changes of Object.values(relawan.changes || {})) {
    let hasChange = 0;
    for (const _ of Object.values(changes)) {
      // TODO: do not count if the data is overriden?
      relawan.numFieldsChanged++;
      hasChange = 1;
    }
    relawan.numRsChanged += hasChange;
  }
  return relawan;
}

export enum REGISTRATION_KEY {
  profesi = 'profesi',
  profesiLainnya = 'profesiLainnya',
  instansi = 'instansi',
  regEmail = 'regEmail',
  noHp = 'noHp',
  fotoIdCard = 'fotoIdCard',
  fotoIdCardMeta = 'fotoIdCardMeta',
  deskripsi = 'deskripsi',
}

export type RegistrationInfo = { [key in REGISTRATION_KEY]: string };

export interface ImageMetadata {
  l: number; // Last Modified Timestamp.
  a: number; // Created Timestamp.
  s: number; // Size in Bytes.
  z: number; // Size in Bytes after compressed.
  w: number; // Original Width.
  h: number; // Original Height.
  m: [string, string]; // [Make, Model].
  o: number; // Orientation.
  y: number; // Latitude.
  x: number; // Longitude.
}

export function ago(ts: number) {
  let elapsed = (Date.now() - ts) / 1000;
  if (elapsed < 60) return Math.floor(elapsed) + 's lalu';
  elapsed /= 60;
  if (elapsed < 60) return Math.floor(elapsed) + 'm lalu';
  elapsed /= 60;
  if (elapsed < 24) return Math.floor(elapsed) + 'h lalu';
  return `${Math.floor(elapsed / 24)}d ${Math.floor(elapsed % 24)}h lalu`;
}

export function extractImageMetadata(m: any): ImageMetadata | null {
  let validM: ImageMetadata | null = null;
  if (m) {
    validM = {} as ImageMetadata;
    ['k', 't', 'l', 'a', 's', 'z', 'w', 'h', 'o', 'y', 'x'].forEach(
      attr => {
        if (typeof m[attr] === 'number') {
          // @ts-ignore
          validM[attr] = m[attr];
        }
      }
    );
    if (typeof m.m === 'object') {
      validM.m = ['', ''];
      if (typeof m.m[0] === 'string') {
        validM.m[0] = m.m[0].substring(0, 50);
      }
      if (typeof m.m[1] === 'string') {
        validM.m[1] = m.m[1].substring(0, 50);
      }
    }
  }
  return !validM || Object.keys(validM).length === 0 ? null : validM;
}

export interface Relawan {
  displayName: string | null;
  email: string | null;
  phoneNumber: string | null;
  photoURL: string | null;
  providerId: string;
  uid: string;

  userid: string;
  nameLowerCase: string;
  role: USER_ROLE;
  registerDate: any; // { seconds: number, nanoseconds: number };

  numRsChanged: number;       // Number of RS that were changed by this user.
  numFieldsChanged: number;   // Number of cells edited by this user.
  numPending: number;         // Number of edit cells that are in pending review.

  registration: RegistrationInfo;

  // Relawan's contributions.
  changes: UserChanges | null;

  // Is the user chose to enable location?
  enable_location: boolean;

  // User location: null means denied.
  location: { lat: number, lng: number } | null;
}

export interface AttributeDetail {
  key: string;
  label: string;
  name?: string;
  longLabel?: string;
  shortLabel: string;
  formLabel: string;
  isPublic: boolean;
  show?: boolean;
  unitLabel?: string;
}

export const Attributes: { [key: string]: AttributeDetail[] } = {
  suspect: [
    // Suspect
    {
      key: 'odp',
      label: 'ODP',
      longLabel: 'Orang Dalam Pemantauan',
      shortLabel: 'ODP',
      formLabel: 'Jumlah Orang Dalam Pemantauan',
      isPublic: false
    },
    {
      key: 'pdpp',
      label: 'PDP Meninggal',
      longLabel: 'Pasien Dalam Pengawasan yang Meninggal',
      formLabel: 'Jumlah Pasien Dalam Pengawasan yang Meninggal',
      shortLabel: 'PDP+',
      isPublic: false
    },
    {
      key: 'pdpr',
      label: 'PDP Dirawat',
      longLabel: 'Pasien Dalam Pengawasan yang Dirawat',
      formLabel: 'Jumlah Pasien Dalam Pengawasan yang Dirawat',
      shortLabel: 'PDP',
      isPublic: false
    },
    {
      key: 'pdps',
      label: 'PDP Sembuh',
      longLabel: 'Pasien Dalam Pengawasan yang Sembuh',
      formLabel: 'Jumlah Pasien Dalam Pengawasan yang Sembuh',
      shortLabel: 'PDP*',
      isPublic: false
    },
    {
      key: 'covp',
      label: 'Positif Covid-19 Meninggal',
      longLabel: 'Pasien Positif Covid-19 yang Meninggal',
      formLabel: 'Jumlah Pasien Positif Covid-19 yang Meninggal',
      shortLabel: 'Cov+',
      isPublic: false
    },
    {
      key: 'covr',
      label: 'Positif Covid-19 Dirawat',
      longLabel: 'Pasien Positif Covid-19 yang Dirawat',
      formLabel: 'Jumlah Pasien Positif Covid-19 yang Dirawat',
      shortLabel: 'Cov',
      isPublic: false
    },
    {
      key: 'covs',
      label: 'Positif Covid-19 Sembuh',
      longLabel: 'Pasien Positif Covid-19 yang Sembuh',
      formLabel: 'Jumlah Pasien Positif Covid-19 yang Sembuh',
      shortLabel: 'Cov*',
      isPublic: false
    },
  ],
  logistic: [
    // APDs and logistics
    {
      key: 'maskSurgical',
      name: 'Surgical Mask',
      shortLabel: 'R.SM',
      label: 'Kebutuhan Surgical Mask',
      formLabel: 'Jumlah Surgical Mask yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'maskn95',
      name: 'N95 Mask',
      shortLabel: 'R.N95',
      label: 'Kebutuhan N95 Mask',
      formLabel: 'Jumlah N95 Mask yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'gloves',
      name: 'Sarung Tangan',
      shortLabel: 'R.ST',
      label: 'Kebutuhan Sarung Tangan',
      formLabel: 'Jumlah Sarung tangan non-steril yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'glovesSteril',
      name: 'Sarung Tangan Steril',
      shortLabel: 'R.STT',
      label: 'Kebutuhan Sarung Tangan Steril',
      formLabel: 'Jumlah Sarung tangan steril yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'glovesLong',
      name: 'Sarung Tangan Panjang',
      shortLabel: 'R.STP',
      label: 'Kebutuhan Sarung Tangan Panjang',
      formLabel: 'Jumlah Sarung tangan panjang (Long Handscoon) yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'cap',
      name: 'Pelindung Kepala (cap)',
      shortLabel: 'R.Cap',
      label: 'Kebutuhan Pelindung Kepala (cap)',
      formLabel: 'Jumlah Pelindung Kepala (cap) yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'goggle',
      name: 'Goggle',
      shortLabel: 'R.Gog',
      label: 'Kebutuhan Goggle',
      formLabel: 'Jumlah Goggle yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'faceShield',
      name: 'Face Shield',
      shortLabel: 'R.FC',
      label: 'Kebutuhan Face Shield',
      formLabel: 'Jumlah Face Shield yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'surgicalGown',
      name: 'Surgical Gown',
      shortLabel: 'R.SG',
      label: 'Kebutuhan Surgical Gown',
      formLabel: 'Jumlah Surgical Gown yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'coverall',
      name: 'Coverall',
      shortLabel: 'R.Coverall',
      label: 'Kebutuhan Coverall',
      formLabel: 'Jumlah Coverall yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'hazmats',
      name: 'Baju Hazmat',
      shortLabel: 'R.Haz',
      label: 'Kebutuhan Baju Hazmat',
      formLabel: 'Jumlah Baju Hazmat yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'apron',
      name: 'Apron',
      shortLabel: 'R.Apr',
      label: 'Kebutuhan Apron',
      formLabel: 'Jumlah Apron yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'boots',
      name: 'Sepatu Boots',
      shortLabel: 'R.Boots',
      label: 'Kebutuhan Sepatu Boots',
      formLabel: 'Jumlah Sepatu Boots yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pasang',
    },
    {
      key: 'shoeCover',
      name: 'Shoe Cover',
      shortLabel: 'R.SC',
      label: 'Kebutuhan Shoe Cover',
      formLabel: 'Jumlah Shoe Cover yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'handSanitizer',
      name: 'Hand Sanitizer',
      shortLabel: 'R.HSan',
      label: 'Kebutuhan Hand Sanitizer',
      formLabel: 'Jumlah Hand Sanitizer yang dibutuhkan',
      isPublic: true,
      unitLabel: 'botol 250cc',
    },
    {
      key: 'handSoap',
      name: 'Sabun Cuci Tangan',
      shortLabel: 'R.HSoap',
      label: 'Kebutuhan Sabun Cuci Tangan',
      formLabel: 'Jumlah sabun cuci tangan yang dibutuhkan',
      isPublic: true,
      unitLabel: 'botol',
    },
    {
      key: 'termometerInfrared',
      name: 'Termometer Infrared',
      shortLabel: 'R.Ter',
      label: 'Kebutuhan Termometer Infrared',
      formLabel: 'Jumlah Termometer Infrared yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'ventilator',
      name: 'Ventilator',
      shortLabel: 'R.Vent',
      label: 'Kebutuhan Ventilator',
      formLabel: 'Jumlah Ventilator yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'vitaminC',
      name: 'Vitamin C',
      shortLabel: 'Vit C',
      label: 'Kebutuhan Vitamin C',
      formLabel: 'Jumlah Vitamin C yang dibutuhkan',
      isPublic: true,
      unitLabel: 'box',
    },
    {
      key: 'vitaminE',
      name: 'Vitamin E',
      shortLabel: 'Vit E',
      label: 'Kebutuhan Vitamin E',
      formLabel: 'Jumlah Vitamin E yang dibutuhkan',
      isPublic: true,
      unitLabel: 'box',
    },
  ],
  bed: [
    // Rooms
    {
      key: 'icu',
      shortLabel: 'ICUs',
      label: 'Kapasitas Tempat Tidur Ruang ICU',
      formLabel: 'Kapasitas ruang ICU di rumah sakit ini',
      isPublic: true
    },
    {
      key: 'isolasi',
      shortLabel: 'Isolations',
      label: 'Kapasitas Tempat Tidur Ruang Isolasi',
      formLabel: 'Kapasitas ruang isolasi di rumah sakit ini',
      isPublic: true
    },
    {
      key: 'kondisi',
      shortLabel: 'Overloaded',
      label: 'Ketersediaan Tempat Tidur',
      formLabel: 'Daya tampung tempat tidur/ruangan',
      isPublic: true
    },
  ],
  availibility: [
    // Availibity tools for support intensive care / isolation
    {
      key: 'availableVentilator',
      shortLabel: 'Av.Vent',
      longLabel: 'Ketersediaan Ventilator',
      label: 'Ventilator',
      formLabel: 'Ketersediaan Non Invasive Ventilator di rumah sakit ini',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'availableMechanicalVentilator',
      shortLabel: 'Av.MVent',
      longLabel: 'Ketersediaan Mechanical Ventilator',
      label: 'Mechanical Ventilator',
      formLabel: 'Ketersediaan Mechanical Ventilator di rumah sakit ini',
      isPublic: true,
      unitLabel: 'unit',
    },
    {
      key: 'availableEcmo',
      shortLabel: 'Av.ECMO',
      longLabel: 'Ketersediaan Extracorporeal Membrane Oxygenation (ECMO)',
      label: 'ECMO',
      formLabel: 'Ketersediaan Extracorporeal Membrane Oxygenation (ECMO) di rumah sakit ini',
      isPublic: true,
      unitLabel: 'unit',
    }
  ]
};
