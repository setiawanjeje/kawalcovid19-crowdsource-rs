import { Injectable } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { BehaviorSubject } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { shareReplay, map, switchMap, take } from 'rxjs/operators';
import { Attributes, Relawan, PlaceData, HistoryValue, toPlaceData, isNumberDataKey, ITEM_STATUS } from './rs/rs.component';
import { UserService } from './user.service';

export interface KvDetail {
  key: string;
  shortLabel: string;

  // Deprecated. Please use "latest" field.
  value: number;

  unitLabel?: string;

  // The latest value of the key.
  latest: HistoryValue<string | number>;
}

export interface RsData {
  placeData: PlaceData;
  suspects: KvDetail[];
  logistics: KvDetail[];
  beds: KvDetail[];
  availabilities: KvDetail[];

  // Jumlah semua kebutuhan di RS ini (termasuk yang mendesak)
  totalKebutuhan: number;

  // Jumlah kebutuhan yang mendesak di RS ini.
  totalMendesak: number;
}

@Injectable({ providedIn: 'root' })
export class UtilService {
  // https://simple.wikipedia.org/wiki/Stellar_classification
  SETELLAR_COLOR = [
    '#9db4ff', // 5m blue
    '#aabfff', // 15m deep blue white
    '#cad8ff', // 45m blue white
    '#ffddb4', // 2h pale yellow orange
    '#ffbd6f', // 6h light orange red
    '#f84235', // 20h scarlet
    '#ba3059', // 2d magenta
    '#605170' // dark purple
  ];
  isHandset = false;
  titleSubject = new BehaviorSubject<string>('Beranda');
  title$ = this.titleSubject.asObservable();

  placeDataById$: Observable<{ [placeId: string]: PlaceData }>;
  allPlaceData$: Observable<RsData[]>;
  allUserData$: Observable<Relawan[]>;

  constructor(
    breakpointObserver: BreakpointObserver,
    private firestore: AngularFirestore,
    private userService: UserService,
  ) {
    breakpointObserver
      .observe(Breakpoints.Handset)
      .subscribe(result => this.isHandset = result.matches);

    this.allUserData$ = this.firestore
      .collection<Relawan>('users')
      .valueChanges()
      .pipe(shareReplay(1));

    const groupKey = {}, shortLabel = {}, unitLabel = {};
    for (let a of Attributes.suspect) {
      groupKey[a.key] = 'suspects';
      shortLabel[a.key] = a.name || a.shortLabel;
    }
    for (let a of Attributes.logistic) {
      groupKey[a.key] = 'logistics';
      shortLabel[a.key] = a.name || a.shortLabel;
      unitLabel[a.key] = a.unitLabel;
    }
    for (let a of Attributes.bed) {
      groupKey[a.key] = 'beds';
      shortLabel[a.key] = a.name || a.shortLabel;
    }
    for (let a of Attributes.availibility) {
      groupKey[a.key] = 'availabilities';
      shortLabel[a.key] = a.name || a.shortLabel;
      unitLabel[a.key] = a.unitLabel;
    }

    const places$ =
      this.firestore
        .collection('rs',
          ref => ref.where('place.types',
            'array-contains-any',
            ['hospital', 'health'])
        )
        .valueChanges()
        .pipe(
          switchMap(async (arr: any[]) => {
            let user = null;
            if (await this.userService.firebaseUser$.pipe(take(1)).toPromise()) {
              user = await this.userService.user$.pipe(take(1)).toPromise();
            }
            return arr.map(placeDataRaw => toPlaceData(placeDataRaw, user));
          }),
          shareReplay(1));

    this.placeDataById$ =
      places$.pipe(
        map((places: PlaceData[]) => {
          const placeDataById: { [placeId: string]: PlaceData } = {};
          for (const pd of places) {
            placeDataById[pd.place.place_id] = pd;
          }
          return placeDataById;
        }),
        shareReplay(1));

    this.allPlaceData$ =
      places$.pipe(
        map((arr: PlaceData[]) => {
          const allData: RsData[] = [];
          for (const placeData of arr) {
            const rsData: RsData = {
              placeData,
              suspects: [],
              logistics: [],
              beds: [],
              availabilities: [],
              totalKebutuhan: 0,
              totalMendesak: 0,
            };
            for (const key of Object.keys(placeData.latest)) {
              if (!groupKey[key]) continue;
              if (!isNumberDataKey(key)) {
                console.warn('Grouping non number', key);
                continue;
              }
              const latest = placeData.latest[key];
              const value = +latest.value;
              if (isNaN(value) || value <= 0) continue; // Ignores empty or invalid values.
              const arr = rsData[groupKey[key]] as KvDetail[];
              arr.push({
                key,
                shortLabel: shortLabel[key],
                value: value,
                unitLabel: unitLabel[key] || '',
                latest,
              });
              rsData.totalKebutuhan += value;
              if (latest.status === ITEM_STATUS.out) {
                rsData.totalMendesak += value;
              }
            }
            if (rsData.totalKebutuhan > 0) allData.push(rsData);
          }
          console.log('hospitals', arr.length, 'punya kebutuhan', allData.length);
          return allData.sort((a, b) => {
            const diff = b.totalKebutuhan - a.totalKebutuhan;
            if (diff) return diff;
            return a.placeData.place.place_id <= b.placeData.place.place_id ? -1 : 1;
          });
        }),
        shareReplay(1));
  }

  getColor(ts: number, type: string = 'bg') {
    if (ts) {
      const c = this.SETELLAR_COLOR;
      const ago = (Date.now() - ts) / 1000 / 60;
      for (let i = 0, t = 5; i < c.length; i++, t *= 3) {
        if (ago <= t) {
          if (type == 'bg') return c[i];
          else {
            if (i > 5) return 'inherit';
          }
        }
      }
    }
    return '';
  }

  setTitle(title: string) {
    this.titleSubject.next(title);
  }

  // Get Location Distance between two coordinates
  // Source: https://www.movable-type.co.uk/scripts/latlong.html
  // @params pointA, pointB, unit: 'K' for kilometers 'M' for meters
  // @return distance by requested unit
  calculateDistance(pointA, pointB, unit = 'K') {
    let radlat1 = Math.PI * pointA.lat / 180;
    let radlat2 = Math.PI * pointB.lat / 180;
    let radlon1 = Math.PI * pointA.lng / 180;
    let radlon2 = Math.PI * pointB.lng / 180;
    let theta = pointA.lng - pointB.lng;
    let radtheta = Math.PI * theta / 180;
    let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit == "K") { dist = dist * 1.609344 }
    if (unit == "M") { dist = dist * 0.8684 }
    return dist;
  }

  timeAgo(ts: number) {
    const current = Date.now();
    const msPerMinute = 60 * 1000;
    const msPerHour = msPerMinute * 60;
    const msPerDay = msPerHour * 24;
    const msPerMonth = msPerDay * 30;
    const msPerYear = msPerDay * 365;

    var elapsed = current - ts;

    if (elapsed < msPerMinute) return 'beberapa detik yang lalu';
    // return Math.round(elapsed/1000) + ' detik yang lalu';
    else if (elapsed < msPerHour) return Math.round(elapsed / msPerMinute) + ' menit lalu';
    else if (elapsed < msPerDay) return Math.round(elapsed / msPerHour) + ' jam lalu';
    else if (elapsed < msPerMonth) return Math.round(elapsed / msPerDay) + ' hari yang lalu';
    else if (elapsed < msPerYear) return Math.round(elapsed / msPerMonth) + ' bulan yang lalu';
    else return Math.round(elapsed / msPerYear) + ' tahun yang lalu';
  }
}
