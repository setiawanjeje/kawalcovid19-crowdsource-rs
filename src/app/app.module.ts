import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';
import { AngularFireStorageModule } from "@angular/fire/storage";

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatRadioModule } from '@angular/material/radio';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { MatMenuModule } from '@angular/material/menu';
import { MatDividerModule } from '@angular/material/divider';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSelectModule } from '@angular/material/select';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { MatBadgeModule } from '@angular/material/badge';
import { ScrollingModule } from '@angular/cdk/scrolling';

// import { ChartsModule } from 'ng2-charts';
import { SocialMediaButtons } from './share/share.module';

import { VizComponent } from './viz/viz.component';
import { RsAddComponent } from './rs-add/rs-add.component';
import { MapComponent } from './map/map.component';
import { environment } from '../environments/environment';
import { MinMaxValidatorComponent } from './rs/min-max-validator.component';
import { RegistrationComponent } from './registration/registration.component';
import { AdminComponent } from './user/admin.component';
import { RsDetailComponent } from './rs-detail/rs-detail.component';
// import { BarChartComponent } from './chart/bar-chart/bar-chart.component';

import { registerLocaleData } from '@angular/common';
import localeId from '@angular/common/locales/id';
import { SingleNumberDialog, SingleNumberCard } from './rs-detail/single-number.component';
registerLocaleData(localeId);

import 'firebase/firestore';
import { RsLogComponent } from './rs-log/rs-log.component';
import { ListComponent } from './list/list.component';
import { HttpClientModule } from '@angular/common/http';
import { PageComponent } from './page/page.component';
import { UserComponent } from './user/user.component';
import { UserLinkComponent } from './user/link.component';
import { UserProfileComponent } from './user/profile.component';
import { UserNotificationComponent } from './user/notification.component';
import { HomeComponent } from './home/home.component';
import { RecentComponent } from './home/recent.component';

@NgModule({
  declarations: [
    AppComponent,
    VizComponent, RsAddComponent, MapComponent, MinMaxValidatorComponent,
    RegistrationComponent, AdminComponent,
    RsDetailComponent, SingleNumberDialog, SingleNumberCard, RsLogComponent,
    // BarChartComponent,
    ListComponent, PageComponent,
    UserComponent,
    UserLinkComponent,
    UserProfileComponent,
    UserNotificationComponent,
    HomeComponent, RecentComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAnalyticsModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,

    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatBadgeModule,
    MatButtonModule,
    MatRadioModule,
    MatIconModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatMenuModule,
    MatDividerModule,
    MatRippleModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatTooltipModule,
    MatSelectModule,
    MatSidenavModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    ScrollingModule,

    // ChartsModule,
    SocialMediaButtons
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
