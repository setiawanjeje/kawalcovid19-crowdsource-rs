import { Component, Input, OnChanges, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, merge, BehaviorSubject } from 'rxjs';
import { map, filter, switchMap, distinctUntilChanged, shareReplay, take } from 'rxjs/operators';
import { PlaceData, toPlaceData, Attributes, AttributeDetail } from '../rs/rs.component';
import { SingleNumberCard } from './single-number.component';
import { UtilService } from '../util.service';
import { UserService } from '../user.service';
import { Location } from '@angular/common';

interface Details {
  placeData: PlaceData;
  filledLogistic: AttributeDetail[];
  unfilledLogistic: AttributeDetail[];
}

@Component({
  selector: 'app-rs-detail',
  templateUrl: './rs-detail.component.html',
  styleUrls: ['./rs-detail.component.css']
})
export class RsDetailComponent implements OnChanges {
  @Input() placeId: string;

  @ViewChild(SingleNumberCard) singleNumberCard: SingleNumberCard;

  placeChangeTrigger$ = new BehaviorSubject<string>('');
  details$: Observable<Details>;
  attributes = Attributes;
  updatingContact = false;

  constructor(
    private route: ActivatedRoute,
    private firestore: AngularFirestore,
    public utilService: UtilService,
    public userService: UserService,
    public location: Location,
  ) {
    this.details$ =
      merge(
        this.route.paramMap.pipe(map((params: ParamMap) => params.get('id'))),
        this.placeChangeTrigger$
      ).pipe(
        filter<string>(Boolean),
        distinctUntilChanged(),
        switchMap((placeId: string) =>
          this.firestore.collection('rs')
            .doc<PlaceData>(placeId)
            .valueChanges()),
        switchMap(async placeDataRaw => {
          let user = null;
          if (await this.userService.firebaseUser$.pipe(take(1)).toPromise()) {
            user = await this.userService.user$.pipe(take(1)).toPromise();
          }
          return toPlaceData(placeDataRaw, user);
        }),
        map((placeData: PlaceData) => {
          const filledLogistic = [];
          const unfilledLogistic = [];
          for (let l of this.attributes.logistic) {
            if (placeData.latest[l.key] || l.show) {
              filledLogistic.push(l);
            } else {
              unfilledLogistic.push(l);
            }
          }
          // Ordering by logistic name
          unfilledLogistic.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
          filledLogistic.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));

          return { placeData, filledLogistic, unfilledLogistic };
        }),
        shareReplay(1)
      );
  }

  ngOnChanges() {
    this.placeChangeTrigger$.next(this.placeId);
  }

  async saveKondisi(placeId: string, value) {
    const res: any = await this.userService.post(`edit/${placeId}/kondisi`, { value });
    if (res.error) {
      alert(res.error);
    }
  }

  openDialog(placeData: PlaceData, l: AttributeDetail) {
    this.singleNumberCard.openDialog({
      title: l.formLabel,
      placeData,
      key: l.key,
      unitLabel: l.unitLabel,
      hold: true,
      hasStatus: true,
    });
  }

  async editContact(placeData: PlaceData) {
    const contact = prompt(
      'Masukkan nomor WA orang yang bisa dihubungi untuk permintaan alat-alat di RS/Puskesmas/Faskes ini:',
      `${placeData.latest.contact?.value}`);
    if (contact && contact !== placeData.latest.contact?.value) {
      this.updatingContact = true;
      try {
        const res: any = await this.userService.post(`edit/${placeData.place.place_id}/contact`, { value: contact });
        if (res.error) {
          alert(res.error);
        }
      } catch (e) {
        alert(e.message);
        console.error(e);
      }
      this.updatingContact = false;
    }
  }
}
