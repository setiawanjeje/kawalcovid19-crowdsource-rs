import { Component, Input, Inject, OnInit, OnChanges, ViewChild, ElementRef, AfterContentInit, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PlaceData, ITEM_STATUS, HistoryValue, ago } from '../rs/rs.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilService } from '../util.service';
import { UserService } from '../user.service';

interface CellHistory extends HistoryValue<number> {
  ago: string;
}

export interface DialogData {
  title: string;
  placeData: PlaceData;
  key: string;
  unitLabel?: string;
  hold: boolean;      // True will not close the dialog until it's submitted.
  hasStatus: boolean; // True will display the item status.
}

@Component({
  selector: 'app-single-number-card',
  template: `
    <mat-card matRipple [matRippleCentered]="true"
      class="text-center" style="cursor: pointer" [style.background-color]="bgcolor"
      (click)="openDialog()">
      <div style="margin-bottom: 0px;">
        <mat-spinner style="margin: 0 auto" [diameter]="35" *ngIf="updating"></mat-spinner>
        <h1 class="lead" *ngIf="!updating">
          {{ placeData.latest[key]?.value || 0 | number:'1.0':'id' }}<span>{{unitLabel}}</span>
        </h1>
        <span class="text-light">{{ subtitle }}</span>
        <p *ngIf="hasStatus && placeData.latest[key]?.status === ITEM_STATUS.out"
          style="color:red; font-weight: bold">(Out Of Stock)</p>
      </div>
    </mat-card>
`,
  styles: [`
    .lead {
      display:flex;
      flex-flow:column;
      font-size: 2.4rem;
      margin:0;
    }
    .lead span{
      font-size:.8rem;
      color:#ccc;
    }
`]
})
export class SingleNumberCard implements OnInit, OnChanges {
  @Input() placeData: PlaceData;
  @Input() key: string;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() unitLabel: string;
  @Input() hold = false;
  @Input() hasStatus = false;

  ITEM_STATUS = ITEM_STATUS;

  bgcolor = '';
  updating = false;

  constructor(
    public dialog: MatDialog,
    public userService: UserService,
    private utilService: UtilService,
  ) { }

  ngOnChanges() {
    this.bgcolor = this.utilService.getColor(this.placeData.latest[this.key]?.ts);
  }

  ngOnInit() {
    this.ngOnChanges();
  }

  // title: string, unitLabel: string, placeData: PlaceData, key: string, value: string, hold = false
  openDialog(data?: DialogData): void {
    if (data === undefined) {
      data = {
        placeData: this.placeData,
        title: this.title,
        unitLabel: this.unitLabel,
        key: this.key,
        hold: this.hold,
        hasStatus: this.hasStatus,
      };
    }

    if (!this.userService.isPublic) {
      console.warn('Only relawan can edit');
      return;
    }

    const dialogRef = this.dialog.open(SingleNumberDialog, {
      maxWidth: '100vh',
      width: this.utilService.isHandset ? '80vh' : '40vh',
      position: { 'bottom': this.utilService.isHandset ? '0px' : '' },
      disableClose: true,
      autoFocus: true,
      data
    });

    dialogRef.afterClosed().subscribe(async result$ => {
      console.log('The dialog was closed', result$);
      if (!result$) return;
      this.updating = true;
      try {
        const result = await result$;
        if (result.error) {
          alert(result.error);
        }
      } catch (e) {
        alert(e.message);
        console.error(e);
      }
      this.updating = false;
    });
  }
}

@Component({
  selector: 'app-single-number-dialog',
  template: `
    <div mat-dialog-content [formGroup]="form">
      <p>{{ data.title }}</p>

      <mat-form-field *ngIf="data.hasStatus">
        <mat-label>Status Barang Sekarang</mat-label>
        <mat-select formControlName="status" (selectionChange)="statusChanged()">
          <mat-option [value]="ITEM_STATUS.short">Short Supply</mat-option>
          <mat-option [value]="ITEM_STATUS.out">Out Of Stock</mat-option>
          <mat-option [value]="ITEM_STATUS.ok">Terpenuhi</mat-option>
        </mat-select>
      </mat-form-field>

      <p [hidden]="data.hasStatus && itemStatus === ITEM_STATUS.ok">
        <mat-form-field>
          <mat-label *ngIf="data.hasStatus">Kebutuhan Tambahan</mat-label>
          <input matInput #input formControlName="value" type="number"
            (ngModelChange)="valueChanged()"
            (keyup.enter)="submit(form.value)" autocomplete="off"
            [disabled]="disabled" cdkFocusInitial>
            <span *ngIf="data.unitLabel" matSuffix style="color:#ccc;margin-left:10px;">{{data.unitLabel}}</span>
        </mat-form-field>
        <app-min-max-validator [control]="form.get('value')" name="Nilai" [maxValue]="MAX2">
        </app-min-max-validator>
        <br>
        <span *ngIf="data.hasStatus" style="color: blue">
          Harap estimasikan untuk kebutuhan tambahan untuk 1 bulan kedepan
        </span>
      </p>
    </div>
    <mat-divider style="margin-left:-24px;margin-right:-24px;"></mat-divider>

    <ng-container *ngIf="userService.isModerator && cellHistory.length > 0">
      <table>
        <caption>Catatan perubahan:</caption>
        <tr><th width="60">Kapan</th><th width="70">Value</th><th>Oleh</th></tr>
        <tr *ngFor="let h of cellHistory">
          <td align="right">{{ h.ago }}</td>
          <td align="center">{{ h.value }} <b>{{ h.status }}</b></td>
          <td><app-user-link [uid]="h.uid" (click)="dialogRef.close()"></app-user-link></td>
        </tr>
      </table>
    </ng-container>

    <div mat-dialog-actions style="justify-content:flex-end;margin-top:0px;">
    <mat-spinner [diameter]="25" *ngIf="disabled"></mat-spinner>
      <button [disabled]="disabled" mat-button (click)="dialogRef.close()">Batal</button>
      <button [disabled]="disabled" mat-raised-button color="primary" type="submit"
        (click)="submit(form.value)">Simpan</button>
    </div>
  `,
})
export class SingleNumberDialog implements AfterContentInit {
  @ViewChild('input', { static: false }) inputRef: ElementRef;

  ITEM_STATUS = ITEM_STATUS;
  disabled = false;
  form: FormGroup;
  MAX2 = 999999;
  cellHistory: CellHistory[] = [];

  constructor(
    public userService: UserService,
    public dialogRef: MatDialogRef<SingleNumberDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
  ) {
    const latest: HistoryValue<string | number> = data.placeData.latest[data.key];
    this.form = this.fb.group({
      status: [latest?.status ?? (data.hasStatus ? ITEM_STATUS.short : ITEM_STATUS.unset)],
      value: [latest?.value ?? '', [Validators.min(0), Validators.max(this.MAX2)]]
    });

    const hs = data.placeData.history;
    for (const h of (hs && hs[data.key])?.reverse() || []) {
      this.cellHistory.push({ ago: ago(h.ts), ...h });
      if (this.cellHistory.length >= 5) break;
    }
  }

  get itemStatus() {
    return this.form.get('status').value;
  }

  ngAfterContentInit() {
    this.cd.detectChanges();
    this.inputRef.nativeElement.select();
  }

  statusChanged() {
    if (this.itemStatus === ITEM_STATUS.ok) {
      this.form.get('value').setValue(0);
    }
  }

  valueChanged() {
    const value = this.form.get('value').value;
    if (value === 0) {
      this.form.get('status').setValue(ITEM_STATUS.ok);
    }
  }

  async submit(value) {
    const placeId = this.data.placeData.place.place_id;
    if (!this.data.hold) {
      this.dialogRef.close(
        this.userService.post(`edit/${placeId}/${this.data.key}`, value));
      return;
    }
    this.disabled = true;
    try {
      const res: any = await this.userService.post(
        `edit/${placeId}/${this.data.key}`, value);
      if (res.error) {
        alert(res.error);
      } else {
        this.dialogRef.close();
      }
    } catch (e) {
      alert(e.message);
      console.error(e);
    }
    this.disabled = false;
  }
}
