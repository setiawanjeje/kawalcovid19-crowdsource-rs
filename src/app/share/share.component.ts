import { Component, ElementRef, ViewChild, AfterViewInit, AfterContentInit, Input } from '@angular/core';
@Component({
    selector: 'share-facebook',
    template: `<a
    href="https://facebook.com/sharer.php?u={{url}}
    &app_id=143146827083282"
    class="facebook-share-button" title="Bagikan ke Facebook">
    <span class="icon-facebook"></span>{{buttonText}}
    </a>`,
    // template: `<a href="http://www.facebook.com/dialog/feed?
    //     app_id=143146827083282&
    //     link={{url}}&
    //     picture={{picture}}&
    //     name={{name}}&
    //     caption={{caption}}&
    //     description={{description}}&
    //     message={{message}}&
    //     redirect_uri={{url}}"><span class="icon-facebook"></span>{{buttonText}}</a>`,

    styles: [`
      .facebook-share-button {
        font-size:2rem;
        line-height:2rem;
        color:#3b5998;
        border-radius:4px;
        border:1px solid #3b5998;
        padding:5px 10px;
        display:flex;
        justify-content:center;
        align-items-center;
      }
      .facebook-share-button:hover {
        background-color:#3b5998;
        color:#fff;
      }
  `]
})

export class FacebookShareComponent implements AfterViewInit {
    @Input() url = location.href;
    @Input() buttonText = '';
    @Input() name = 'Bagikan Kawal Rumah Sakit';
    @Input() caption = 'Kawal kebutuhan APD dan Faskes';
    @Input() description = 'Mari kita bangun komunikasi dengan nakes untuk kawal kebutuhan APD dan Faskes (fasilitas kesehatan lainnya) di Fasyankes (Rumah Sakit/Puskesmas/Klinik) seluruh Indonesia';
    @Input() message = 'Yuk kawal kebutuhan APD dan Faskes';
    @Input() picture = 'https://kawalrumahsakit.id/assets/images/campaign_kawalrumahsakit.id_1.png';
    constructor() {

    }

    ngAfterViewInit(): void {

    }
}

@Component({
    selector: 'share-twitter',
    // template: `<a href="https://twitter.com/share" [attr.data-text]="text" [attr.data-url]="url" class="twitter-share-button">{{buttonText}}</a>`,
    template:`<a class="twitter-share-button"
            href="https://twitter.com/intent/tweet?text={{text}} {{url}}" title="Kicaukan di Twitter">
            <span class="icon-twitter"></span>{{buttonText}}</a>`,
    styles: [`
      .twitter-share-button {
        font-size:2rem;
        line-height:2rem;
        color:#3587FA;
        border-radius:4px;
        border:1px solid #3587FA;
        padding:5px 10px;
        display:flex;
        justify-content:center;
        align-items-center;
      }
      .twitter-share-button:hover {
        background-color:#3587FA;
        color:#fff;
      }
  `]
})

export class TwitterShareComponent implements AfterViewInit {
    @Input() url = location.href;
    @Input() text = '';
    @Input() buttonText = '';

    constructor() {
        // load twitter sdk if required
        // const url = 'https://platform.twitter.com/widgets.js';
        // if (!document.querySelector(`script[src='${url}']`)) {
        //     let script = document.createElement('script');
        //     script.src = url;
        //     document.body.appendChild(script);
        // }
    }

    ngAfterViewInit(): void {
        // render tweet button
        // window['twttr'] && window['twttr'].widgets.load();
    }
}


@Component({
    selector: 'share-whatsapp',
    template: `<a href="https://api.whatsapp.com/send?text={{encodedText}}"
    data-action="share/whatsapp/share"
    class="whatsapp-share-button"
    title="Bagikan melalui WhatsApp"><span class="icon-whatsapp"></span>{{buttonText}}</a>`,
    styles: [`
      .whatsapp-share-button {
        font-size:2rem;
        line-height:2rem;
        color:#70D467;
        border-radius:4px;
        border:1px solid #70D467;
        padding:5px 10px;
        display:flex;
        justify-content:center;
        align-items-center;
      }
      .whatsapp-share-button:hover{
        background-color:#70D467;
        color:#fff;
      }
  `]
})

export class WhatsAppShareComponent implements AfterContentInit {
    @Input() url = location.href;
    @Input() text = '';
    @Input() buttonText = '';
    encodedText:string;
    constructor() {

    }
    ngAfterContentInit(): void {
      this.encodedText = `${encodeURIComponent(this.text)}${encodeURIComponent(' ')}${encodeURIComponent(this.url)}`;
    }
}


@Component({
    selector: 'share-googleplus',
    template: `<div class="gplus-share-button" [attr.data-href]="url" data-size="medium">{{buttonText}}</div>`
})

export class GooglePlusShareComponent implements AfterViewInit {
    @Input() url = location.href;
    @Input() buttonText = 'Bagikan ke G+';
    constructor() {
        // load google plus sdk if required
        const url = 'https://apis.google.com/js/platform.js';
        if (!document.querySelector(`script[src='${url}']`)) {
            let script = document.createElement('script');
            script.src = url;
            document.body.appendChild(script);
        }
    }

    ngAfterViewInit(): void {
        // render google plus button
        window['gapi'] && window['gapi'].plusone.go();
    }
}


@Component({
    selector: 'share-linkedin',
    template: `<div #element></div>`
})

export class LinkedInShareComponent implements AfterViewInit {
    @Input() url = location.href;
    @ViewChild('element') element: ElementRef;

    constructor() {
        // load twitter sdk if required
        const url = 'https://platform.linkedin.com/in.js';
        if (!document.querySelector(`script[src='${url}']`)) {
            let script = document.createElement('script');
            script.src = url;
            script.innerHTML = ' lang: en_US';
            document.body.appendChild(script);
        }
    }

    ngAfterViewInit(): void {
        // add linkedin share button script tag to element
        this.element.nativeElement.innerHTML = `<script type="IN/Share" data-url="${this.url}"></script>`;

        // render share button
        window['IN'] && window['IN'].parse();
    }
}
