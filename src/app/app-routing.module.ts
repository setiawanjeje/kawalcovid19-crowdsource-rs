import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VizComponent } from './viz/viz.component';
import { RsAddComponent } from './rs-add/rs-add.component';
import { RegistrationComponent } from './registration/registration.component';
import { AuthGuardService } from './auth-guard.service';
import { AdminComponent } from './user/admin.component';
import { RsDetailComponent } from './rs-detail/rs-detail.component';
import { ListComponent } from './list/list.component';
import { PageComponent } from './page/page.component';
import { UserComponent } from './user/user.component';
import { UserProfileComponent } from './user/profile.component';
import { UserNotificationComponent } from './user/notification.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: 'rs/:id', component: RsDetailComponent, data: { title: 'Detail RS/Faskes' } },
  { path: 'rs', component: RsAddComponent, canActivate: [AuthGuardService], data: { title: 'Temukan RS/Faskes' } },
  { path: 'viz', component: VizComponent, data: { title: 'Visualisasi' } },
  { path: 'list', component: ListComponent, data: { title: 'Daftar RS/Faskes' } },
  { path: 'login', component: RegistrationComponent, data: { title: 'Akun' } },
  { path: 'page/:slug', component: PageComponent, data: { title: 'page' } }, // it will be procecced on app.component.ts
  { path: 'home', component: HomeComponent, data: { title: 'Beranda' } },
  {
    path: 'user', component: UserComponent, data: { title: 'User Page' },
    children: [
      { path: 'profile', component: UserProfileComponent },
      { path: 'notification', component: UserNotificationComponent },
      {
        path: 'admin', component: AdminComponent, data: { title: 'User Management' },
        children: [
          { path: 'u/:id', component: UserProfileComponent },
        ],
      },
      { path: '**', redirectTo: 'profile', pathMatch: 'full' },
    ]
  },
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
