import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MapService } from '../map/map.service';
import { UserService } from '../user.service';
import { Observable, Subject, merge } from 'rxjs';
import { map, distinctUntilChanged, switchMap, shareReplay } from 'rxjs/operators';
import { PlaceData } from '../rs/rs.component';

@Component({
  selector: 'app-rs-add',
  templateUrl: './rs-add.component.html',
  styleUrls: ['./rs-add.component.css']
})
export class RsAddComponent implements AfterViewInit {
  @ViewChild('pacInput', { static: false }) pacInput: ElementRef;
  @ViewChild('anchor', { static: false }) anchor: ElementRef;

  placeDetailTrigger$ = new Subject<string>();
  place$: Observable<google.maps.places.PlaceResult>;

  constructor(
    private firestore: AngularFirestore,
    private userService: UserService,
    private mapService: MapService,
  ) { }

  ngAfterViewInit() {
    this.place$ = merge(
      this.placeDetailTrigger$,
      this.mapService
        .autocomplete$(this.pacInput.nativeElement, { componentRestrictions: { 'country': 'id' } })
        .pipe(map(place => place.place_id))
    ).pipe(
      distinctUntilChanged(),
      switchMap(placeId =>
        this.firestore.collection('rs')
          .doc<PlaceData>(placeId)
          .valueChanges()
          .pipe(switchMap(async placeData =>
            placeData?.place ? placeData.place : this.userService
              .post<google.maps.places.PlaceResult>(`place_details/${placeId}`, {})))
      ),
      map(place => {
        this.pacInput.nativeElement.value = place.name;
        return place;
      }),
      shareReplay(1)
    );
  }

  scrollIntoView() {
    this.anchor.nativeElement.scrollIntoView({ behavior: "smooth", block: "end" });
  }
}
